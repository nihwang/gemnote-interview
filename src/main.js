const setActiveTab = (tab) => {
  document.querySelector('.is-active').classList.remove('is-active');
  tab.classList.add('is-active');
};

const tabsEventListener = () => {
  const tabs = document.querySelectorAll('.tabs li a');
  for (let i = 0; i < tabs.length; i += 1) {
    const tab = tabs[i];
    tab.addEventListener('click', () => setActiveTab(tab));
  }
};

const showExtraForm = () => {
  const formToShow = document.querySelector('.form-to-show');
  formToShow.classList.add('show-form');
};

const inputEventListener = () => {
  const lastRequiredInput = document.querySelector('.last-required-input');
  lastRequiredInput.addEventListener('input', () => showExtraForm());
};

const onReady = () => {
  tabsEventListener();
  inputEventListener();
};

document.addEventListener('DOMContentLoaded', onReady);
