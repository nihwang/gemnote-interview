const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: {
    styles: './src/style.scss',
    index: './src/index.html',
    main: './src/main.js',
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, './dist'),
  },
  module: {
    rules: [
      { test: /\.js$/, use: 'babel-loader' },
      { test: /\.html$/, use: ['file-loader?name=[name].html'] },
      { test: /\.scss$/, use: ['file-loader?name=[name].css', 'sass-loader'] },
    ],
  },
  plugins: [
    new CopyWebpackPlugin([{ from: 'assets', to: 'assets' }]),
  ],
  watchOptions: {
    ignored: /node_modules/,
  },
};
